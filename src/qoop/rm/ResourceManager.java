package qoop.rm;

import java.util.logging.*;
import java.util.Random;

public class ResourceManager {
    
    private static final Logger L = Logger.getLogger(ResourceManager.class.getName());
 
    int cluster_size;
    int[] cluster_status;
    String scheduler_policy;

    public ResourceManager(int cluster_size, String scheduler_policy) {
        this.cluster_size = cluster_size;
        this.cluster_status = new int[cluster_size + 1];
        this.scheduler_policy = scheduler_policy;
    }

    public int getResource() {
        int allotted_machine_id;
        if (scheduler_policy.equals("random")) {
            Random rand;
            allotted_machine_id = rand.nextInt(cluster_size) + 1;
            cluster_status[allotted_machine_id] = 1;
        }
        return allotted_machine_id;
    }

    public void loseResource(int machine_id) {
        cluster_status[machine_id] = 0;
    }
}
