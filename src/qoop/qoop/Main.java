package qoop.qoop;

import java.util.logging.*;
import qoop.data.Dataset;
import qoop.rm.ResourceManager;
import qoop.qo.QueryOptimizer;

public class Main {

    private static Logger L = Logger.getLogger(Main.class.getName());
   
    // should be private - public for now
    public Dataset[] dataset;
    public ResourceManager rm;
    public QueryOptimizer qo;

    public Main() {
        int dataset_size = 10;
        int cluster_size = 10;
        dataset = new Dataset[dataset_size];
        for (int i = 0; i < dataset_size; i++) {
            // data_id, size, min, max, replica_factor, cluster_size
            dataset[i] = new Dataset(i, 100, 5, 11, 3, cluster_size);
        }
        rm = new ResourceManager(10, "random");
    }

    public static void main(String[] args) {
        L.info("*** QooP Simulator Begins ***");
        Dataset test = new Dataset(100, 5, 11);
        test.printDataset();
    }
}
