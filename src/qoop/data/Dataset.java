package qoop.data;

import java.util.logging.*;
import java.util.Random;
import java.lang.Math;

public class Dataset {
 
    private static final Logger L = Logger.getLogger(Dataset.class.getName());
    
    // should be private - public for now
    public int data_id;
    public int size;
    public int min;
    public int max;
    public int range;
    public int[] data;
    public int replica_factor;
    public int[] replica_location;
    public int[] replica_in_memory;
    public int cluster_size;

    public Dataset(int data_id, int size, int min, int max, int replica_factor, int cluster_size) {
        this.data_id = data_id;
        this.size = size;
        this.min = min;
        this.max = max;
        this.range = max - min + 1;
        this.data = new int[range];
        this.generateData(size);
        this.replica_factor = replica_factor;
        this.cluster_size = cluster_size;
        replica_location = new int[replica_factor];
        replica_in_memory = new int[replica_factor];
        Random rand;
        
        // generate replicas
        for (int i = 0; i < replica_factor; i++) {
            boolean accept = false;
            int replica_machine_id;
            while (accept == false) {
                replica_machine_id = rand.nextInt(cluster_size) + 1;
                accept = true;
                for (int j = 0; j < i; j++) {
                    if (replica_locations[j] == replica_machine_id) 
                        accept = false;
                }
            }
            replica_location[i] = replica_machine_id;
            if (Math.random() > 0.5)
                replica_in_memory[i] = 1;
        }
    }

    public void generateData(int size) {
        for (int i = 0; i < size; i++) {
            int num = 0;
            for (int j = 0; j < (range-1); j++) {
                if (Math.random() > 0.5)
                    num++;
            }
            data[num] += 1;
        }
    }

    public void printDataset() {
        for (int i = 0 ; i < range; i++) {
            L.info((min + i) + ":" + data[i] + ",");
        }
    }
}
