package qoop.qo;

import java.util.logging.*;

import qoop.data.Dataset;
import qoop.data.Main;

public class QueryOptimizer {
    
    private static Logger L = Logger.getLogger(QueryOptimizer.class.getName());
    
    String type; // static or dynamic
    int query_size;
    int current_stage;
    int[] dag;

    public QueryOptimizer(String type, int query_size) {
        this.type = type;
        this.query_size = query_size;
        current_stage = 0;
        dag = new int[query_size];
        if (this.type.equals("static")) {
            for (int i = 0; i < query_size; i++) {
                dag[i] = i;
            }
        }
    }

    public int getNextStage(int machine_id) {
        int next_stage;
        if (type.equals("static")) {
            next_stage = dag[current_stage];
            current_stage++;
        } else {
            // find best next stage
            boolean is_local = false;
            boolean is_in_memory = false;
            for (int i = 0; i < query_size; i++) {
                boolean is_scheduled = false;
                for (int j = 0; j < current_stage; j++) {
                    if (i == dag[j]) {
                        is_scheduled = true;
                        break;
                    }
                }
                if (is_scheduled == false) {
                    Dataset in_consideration = Main.dataset[i];
                    for (int k = 0; k < in_consideration.replica_factor; k++) {
                        if (in_consideration.replica_location[k] == machine_id) {
                            next_stage = i;
                            is_local = true;
                            if (in_consideration.replica_in_memory[k] == 1) {
                                is_in_memory = true;
                                break;
                            }
                        }
                    }
                    if (is_local == true && is_in_memory == true) 
                        break;
                    if (is_local == false)
                        next_stage = i;
                }
            }
        }
        return next_stage;
    }
}
